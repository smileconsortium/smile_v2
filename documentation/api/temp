    <div class="page-header"></div>
<!--Begin Method Details: PUT /user/{user} -->
    <p class="lead class-name method-name put"><span class="method-type">PUT</span> /user/{user}</p>

    <p class="det">Updates user. Only some fields may be edited. If a field is not in the User schema, then by default it can still be added to or modified on a User object. However, fields that are not present in the request schema but that are part of the User schema cannot be added nor updated. As with creating a user, new username or email addresses must be unique.<br>An authenticated user can only update their own User object. Admins can update any User. {user} represents the user's UUID. To update a user's password, call the update password function.</p>
    Request Schema
         <pre class="prettyprint linenums">
{
    username: "noah",
    firstName: "Noah",
    lastName: "Freedman,
    emailAddresses: [ "freedmannoah@gmail.com", "noahfreedman@stanford.edu" ]
    country: "United States",
    region: "California",
    city: "Carmel",
    roles: ["student"],
    grade: "12",
    institutions: ["1234i1288", "12349001"],
    language: "English"
}</pre>
    Response Schema
         <pre class="prettyprint linenums">
{
    success: true,
    [error]: { code: 1, msg: "error message" }
}</pre>
    Error Codes
    <table class="table table-bordered">
        <thead>
        </thead>
        <tbody>
        <tr>
            <td><b>HTTP Status Code</b></td>
            <td><b>SMILE Error Code</b></td>
            <td><b>Error message</b></td>
            <td><b>Reason</b></td>
        </tr>
        <tr class="error-summary">
            <td class="error-code">400</td>
            <td class="smile-code">2</td>
            <td class="error-msg">Input validation error: [username]</td>
            <td class="error-desc">One or more of the inputs did not meet validation requirements.</td>
        </tr>
        <tr class="error-summary">
            <td class="error-code">401</td>
            <td class="smile-code">6</td>
            <td class="error-msg">Admin authorization is required to modify another user's details.</td>
            <td class="error-desc">User must be authenticated as an admin user to update a user other than themself.</td>
        </tr>
        <tr class="error-summary">
            <td class="error-code">400</td>
            <td class="smile-code">4</td>
            <td class="error-msg">Username already exists: [username]</td>
            <td class="error-desc">Username already exists at this host (or at global host as well if internet is available from ad-hoc server.</td>
        </tr>
        <tr class="error-summary">
            <td class="error-code">400</td>
            <td class="smile-code">5</td>
            <td class="error-msg">Email address already exists: [email address]</td>
            <td class="error-desc">Username already exists at this host (or at global host as well if internet is available from ad-hoc server.</td>
        </tr>
        </tbody>
    </table>
<!--End Method Details: PUT /user/{user} -->