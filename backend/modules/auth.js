/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */
 
$.using(
	'system.crypto'
);

/**
 * Handles authentication requests, according to the following requirements:
 *
 * Request validators:
 *	1. Input is required in order to continue.
 *	2. The current client must not be authenticated.
 *
 * Input validators:
 *	1. Either property "username" or "email" is required.
 *	2. Property "password" is required.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('POST', '/auth/login', ['smile-no-session', 'smile-input'], function() {

	// Extract the data from the body payload
	var body = this.request.body;
	var user = body.username;
	var email = body.email;
	var password = body.password;
	var type, login;
	
	// Make sure the user and email are not specified at the same time
	if(user && email) {
		return this.response.sendError(400, 1, 
			'Only one of these fields required at a time: ["username", "email"].'
		);
	}
	
	// Determine the authentication type and login value
	if(user) {
		type = 'username';
		login = user;
	} else {
		type = 'emailAddresses',
		login = email;
	}
	
	// Run input validation
	if(!login || !password) {
		return this.response.sendError(400, 1,
			'Required request fields missing: ["username" | "email", "password"]'
		);
	}
	
	// Find the user by login/password
	var view = 'by_' + type;
	
	this.couchDB.get('user', view, { key: login },
			this, function(records) {
		
		if(records.length === 0) {
		
			// Build the 404 error message
			var code, message;
			
			if(type === 'username') {
				code = 9;
				message = 'Username not found: "' + login + '"';
			} else {
				code = 10;
				message = 'Email address not found: "' + login + '"';
			}
			
			// Send the error message
			this.response.sendError(404, code, message);
			return;
		
		}
		
		// Get the user and hash the given password
		var user = records[0];
		password = $.crypto.sha1(password);
		
		// Handle password mismatch
		if(user.password !== password) {
			this.response.sendError(401, 11, 'Incorrect password.');
			return;
		}
		
		// Start the user session
		var session = this.session.data;
		session.id_user = user._id;
		session.user = user;
		session.admin = user.admin;
		session.authenticated = true;
		session.time_authenticated = new Date();
		
		// Regenerate the user session in order to prevent session hijacking
		// attacks.
		this.session.regenerate();
		
		// Send the success response
		this.response.sendUUID(user._id);
		
	});

}); 

/**
 * Handles authentication requests, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('POST', '/auth/logout', ['smile-session'], function() {

	// Reset user session
	var session = this.session.data;
	delete session.id_user;
	delete session.user;
	delete session.admin;
	delete session.time_authenticated;
	
	// Change the authenticated flag
	session.authenticated = false;

	this.response.sendOK();
});

