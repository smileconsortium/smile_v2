/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

    'system.object',
    'application.modules.schema.session'

);

/**
 * Fetches the session by an owning group or user and a specific key and
 * sends a response with the final sanitized list.
 *
 * @param string doctype
 *    The doctype matching the field to filter for.
 *
 * @param string id
 *    The document ID of the specified doctype to filter for.
 */
function run_fetch_by_view(view, id) {

    var keys = {};

    if (typeof(id[id.length - 1]) === 'boolean') {
        keys.key = id;
    } else {
        //if visible isn't specified, need to search using startkey and endkey
        keys.startkey = id;
        var endKey = id.slice();
        endKey.push({});
        keys.endkey = endKey;
    }

    this.couchDB.get('session', view, keys, this, function (records) {
        // Sanitize all records
        for (var i in records) {
            records[i] = this.sanitize(records[i]);
        }

        this.response.send(200, {
            success: true,
            sessions: records
        });

    });


}

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *    The validation report.
 *
 * @return mixed[]
 *    An array containing the response status, code and message.
 */
function build_validation_report(report) {

    // Build the error response
    var validators = report.error.byValidators;
    var code, message;

    if (validators.required) {

        code = 1;
        message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';

    } else {

        code = 2;
        message = 'Input validation error: ["' +
            report.error.attributes.join('", "') + '"]';

    }

    return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *    The validation error report to build the response from.
 */
function send_validation_report(report) {

    // Send the error response
    var report = build_validation_report.call(this, report);
    this.response.sendError(report[0], report[1], report[2]);

}

/**
 * Creates a new session instance.
 */
$.app.handle('POST', '/session', ['smile-session', 'smile-input'], function () {

    // Get input and session data
    var input = this.request.body;
    var session = this.session.data;
    var me = session.id_user;

    // Validate the given input
    $.schemaSession.validator.validate('POST', input, this, function (report, document) {

        // Report any validation errors
        if (!report.success) {
            send_validation_report.call(this, report, document);
            return;

        }

        // Post the new document to CouchDB
        var _do_post = function () {
            var document = $.object.extend({}, $.schemaSession.template, input);
            var now = new Date();
            document.creator = me;
            document.createdOn = [
                now.getFullYear(),
                now.getMonth() + 1,
                now.getDate(),
                now.getHours(),
                now.getMinutes(),
                now.getSeconds()
            ];

            this.couchDB.store(document, this, function (success, id, revision) {

                if (success) {
                    this.response.sendUUID(id);
                } else {
                    $.error('InstitutionModule: failed to store document in couchdb.');
                    this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                }

            });

        };

        if (!session.admin) {
            // Find the specified group
            this.couchDB.get('group', "by_id", { key: input.group }, this, function (groups) {

                var group = groups && groups[0];

                if (!group) {
                    this.response.send(404, {
                        success: false,
                        error: {
                            code: 13,
                            msg: 'Session\'s group does not exist. No group found with UUID: ' + input.group
                        }
                    });

                    return;
                }

                if (group.owner !== me &&
                    (!group.organizerIds || group.organizerIds.indexOf(me) < 0)) {

                    this.response.sendError(403, 27,
                        'Admin authorization required to set the session\'s group to a group in which you are not an organizer or owner.'
                    );

                    return;
                }

                _do_post.call(this);

            });

        } else {
            _do_post.call(this);
        }

    });

});

/**
 * Creates a new session instance.
 */
$.app.handle('PUT', '/session/%w', ['smile-session', 'smile-input'], function (uuid) {

    // Get input and session data
    var input = this.request.body;
    var session = this.session.data;
    var me = session.id_user;

    // Find the current record
    this.find('session', uuid, this, function (record) {

        // Perform the necessary validations before posting
        var _do_post = function () {

            // Avoid updating with a NULL value
            if (!input.group && input.group === null) {
                delete input.group;
            }

            if (!input.activity && input.activity === null) {
                delete input.activity;
            }

            // Disallow changes to group and activity
            if (!session.admin) {

                if (input.activity && input.activity !== record.activity) {

                    this.response.sendError(403, 28,
                        'Admin authorization required to change a session\'s activity.'
                    );

                    return;

                }

                if (input.group && input.group !== record.group) {

                    this.response.sendError(403, 29,
                        'Admin authorization required to change a session\'s group.'
                    );

                    return;

                }

            }

            // Build the new document version and post it
            var newDoc = $.object.extend({}, $.schemaSession.template, record, input);

            // Validate the given input
            $.schemaSession.validator.validate('PUT', newDoc, this, function (report, document) {

                // Report any validation errors
                if (!report.success) {
                    send_validation_report.call(this, report, document);
                    return;

                }
                this.couchDB.store(newDoc, this, function (success, id, revision) {
                    if (success) {
                        this.response.sendOK();
                    } else {
                        this.response.sendError(500, -1, 'Failed to post to CouchDB.');
                    }

                });
            });

        };

        // Admins can change any session
        if (session.admin) {
            _do_post.call(this);
            return;
        }

        // Find the session group to make sure the current user can
        // change it.
        this.find('group', record.group, this, function (grecord) {

            // Make sure the current user is an organizer
            if (grecord.owner !== me &&
                (!grecord.organizerIds || grecord.organizerIds.indexOf(me) < 0)) {

                this.response.sendError(403, 30,
                    'You must be an admin or organizer in the session\'s group to update the session.'
                );

                return;

            }

            _do_post.call(this);

        });

    });


});

/**
 * Deletes a session instance.
 */
$.app.handle('DELETE', '/session/%w', ['smile-session'], function (uuid) {

    // Authentication is required
    var session = this.session.data;
    var me = session.id_user;

    // Find the record to be deleted
    this.find('session', uuid, this, function (record) {

        // Delete the existing record
        var _do_delete = function () {

            this.couchDB.delete(record, this, function (success) {

                if (success) {
                    this.response.sendOK();
                } else {
                    this.response.sendError(500, -1, 'Failed to delete from CouchDB.');
                }

            });

        };

        // Admins can delete it without further validations
        if (session.admin) {
            _do_delete.call(this);
            return;
        }

        // Find the owning group
        this.find('group', record.group, this, function (grecord) {

            // Make sure the current user is an organizer
            if (grecord.owner !== me &&
                (!grecord.organizerIds || grecord.organizerIds.indexOf(me) < 0)) {

                this.response.sendError(403, 30,
                    'Only admins or organizers of a session\'s group can call this function.'
                );

                return;

            }

            _do_delete.call(this);
        });

    });
});

/**
 * Fetches a session instance.
 */
$.app.handle('GET', '/session/%w', ['smile-session'], function (uuid) {
    // Get the session data
    var session = this.session.data;
    var me = session.id_user;

    // Find the session
    this.find('session', uuid, this, function (record) {

        if (session.admin) {
            this.response.send(200, {
                success: true,
                session: this.sanitize(record)
            });
        } else {
            // Find the owning group
            this.find('group', record.group, this, function (grecord) {

                // Make sure the current user is an organizer
                if (grecord.memberIds.indexOf(me) < 0) {

                    this.response.sendError(403, 30,
                        'You must be a member in the session\'s group to view that session.'
                    );

                    return;

                }

                this.response.send(200, {
                    success: true,
                    session: this.sanitize(record)
                });

            });
        }
    });

});

/**
 * Fetches all activity sessions.
 */
$.app.handle('POST', '/session/getSessions', ['smile-session', 'smile-input'], function () {
    // Authentication is required
    var input = this.request.body;
    var session = this.session.data;
    var me = session.id_user;

    var activity = input.activity;
    var group = input.group;


    if (!group) {
        if (!session.admin) {
            this.response.sendError(403, 31, 'Only admins can request all sessions for an activity not filtered by group. Please specify a group.');
            return;
        }

        if (!activity) {
            this.response.sendError(403, 31.1, 'This function requires that either a group or activity be provided in the request.');
            return;
        }
        //Search only by activity (admin only)
        run_fetch_by_view.call(this, 'by_activity', [activity]);
        return;
    } else {
        this.couchDB.get('group', "by_id", { key: group }, this, function (groups) {
            var groupDoc = groups && groups[0];

            if (!groupDoc) {
                this.response.send(404, {
                    success: false,
                    error: {
                        code: 13,
                        msg: 'Specified group does not exist. No group found with UUID: ' + group
                    }
                });

                return;
            }

            if (session.admin || groupDoc.owner === me || groupDoc.organizerIds.indexOf(me) > -1) {
                //Retrieve all sessions, including hidden sessions

                if (!activity) {
                    run_fetch_by_view.call(this, 'by_group', [group]);
                } else {
                    run_fetch_by_view.call(this, 'by_group_activity', [group, activity]);
                }
            } else {
                if (groupDoc.memberIds.indexOf(me) > -1) {
                    //Only retrieve visible sessions
                    if (!activity) {
                        run_fetch_by_view.call(this, 'by_group', [group, true]);
                    } else {
                        run_fetch_by_view.call(this, 'by_group_activity', [group, activity, true]);
                    }
                } else {
                    this.response.send(404, {
                        success: false,
                        error: {
                            code: 30,
                            msg: 'You must be a member of this group to view it\'s sessions. Group UUID: ' + group + '.'
                        }
                    });
                }
            }
        });
    }

});