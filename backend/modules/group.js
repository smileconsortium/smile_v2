/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(
	
	'system.object',	
	'application.modules.schema.group',
    'system.array'
	
);

/**
 * Sanitizes the group document for display.
 *
 * @param object document
 *	The document to be sanitized.
 *
 * @return object
 *	The sanitized document.
 */
function sanitize_group_document(document) {

	// Define the photo URL
	if(document._attachments && document._attachments['photo']) {
		document.photoUrl = this.couchDB.baseUrl + '/' + document._id + '/photo';
	}
	
	return this.sanitize(document);

};

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *	The validation report.
 *
 * @return mixed[]
 *	An array containing the response status, code and message.
 */
function build_validation_report(report) {
	
	// Build the error response
	var validators = report.error.byValidators;
	var code, message;
	
	if(validators.required) {
		
		code = 1;
		message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';
		
	} else if(validators.unique) {
		
		if(validators.unique.indexOf('name') > -1) {
		
			code = 16;
			message = 'Group name already exists.';
			
		} else {
			code = 2;
			message = 'Unique value already exists: ["' +
				validators.unique.join('", "') + '"]';
		}
		
	} else {
	
		code = 2;
		message = 'Input validation error: ["' +
			report.error.attributes.join('", "') + '"]';
			
	}
	
	return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *	The validation error report to build the response from.
 */
function send_validation_report(report) {

	// Send the error response
	var report = build_validation_report.call(this, report);
	this.response.sendError(report[0], report[1], report[2]);
	
}

/**
 * Verifies whether or not the group name is unique
 */
$.app.handle('GET', '/group/nameIsUnique', ['smile-session'], function() {
	
	// Name is required
	var name = this.request.query.name;
	
	if(!name) {
		this.response.sendError(400, 1,
			'Missing required fields in request query string: ["name"]'
		);
		
		return;
	}
	
	// Find the record by name
	this.couchDB.get('group', 'by_name', { key: name }, this, function(records) {
	
		this.response.send(200, {
			success: true,
			unique: (records.length === 0)
		});
	
	});
	
});

/**
 * Fetches an existing group by name.
 */
$.app.handle('GET', '/group/getByName/%s', ['smile-session'], function(name) {
	
	// Find the old record
	this.couchDB.get('group', 'by_name', { key: name }, this, function(records) {
		
		var record = records[0];
		
		if(record) {
			this.response.send(200, {
				success: true,
				group: sanitize_group_document.call(this, record)
			});
		} else {
			this.response.sendError(404, 14, 'No "group" found with "name": ' + name);
		}
		
	});
	
});

/**
 * Adds members to an existing group.
 **/
$.app.handle('PUT', '/group/%w/addMembers', ['smile-session', 'smile-input'], function(uuid) {
		
	// Get the request session and input
	var session = this.session.data;
	var me = session.id_user;
	var input = this.request.body;
	console.log(input);
	// Get the given members
	var members = input.members;
	
	if(!(members instanceof Array)) {
	
		this.response.sendError(400, 1,
			'Required fields in request are missing: ["members"]'
		);
		
		return;
		
	}
	
	for (var i in members) {
	
		if (!members[i].UUID) {

			this.response.sendError(400, 2, 'Input validation error: ["members"]');
			return;
			
		}
	
	}
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		// Admin authorization required to add members to a group in which you 
		// are not an organizer.
		if(!session.admin && record.owner !== session.id_user &&
			(!record.organizerIds || 
				record.organizerIds.indexOf(session.id_user) < 0)) {
			
			this.response.sendError(401, 7,
				'Admin authorization required to call this function.'
			);
			
			return;
				
		}
		
		// Get the group data as actual arrays
		var gmembers = (record.memberIds instanceof Array) ?
			record.memberIds : [];
			
		var gorganizers = (record.organizerIds instanceof Array) ?
			record.organizerIds : [];
		
		// Go through all entries
		var length = members.length;
		for(var i = 0; i < length; ++i) {
		
			var member = members[i];
			var muid = member.UUID;
			
			// Add the member new member id
			if(gmembers.indexOf(muid) < 0) {
				gmembers.push(muid);
			}
			
			// Add him as an organizer as well
			if(member.organizer && gorganizers.indexOf(muid) < 0) {
			
				gorganizers.push(muid);
			}
		
		}
		
		// Update the record
		record.memberIds = gmembers;
		record.organizerIds = gorganizers;
		
		if(length > 0) {
		
			// Post the changes to CouchDB
			this.couchDB.store(record, this, function(success, id, revision) {
			
				if(success) {
					this.response.sendOK();
				} else {
					this.response.sendError(500, -1, 'Failed to post to CouchDB.');
				}
			
			});
		
		} else {
			this.response.sendOK();
		}
		
	});

});

/**
 * Removes members from an existing group.
 */
$.app.handle('PUT', '/group/%w/removeMembers', ['smile-session', 'smile-input'], function(uuid) {
		
	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Validate user input
	var input = this.request.body.members;
	
	if (!(input instanceof Array)) {
		
		this.response.sendError(400, 1, 'Required fields in request are missing: ["members"]');
		return;
		
	}
	
	for (var i in input) {
	
		if (!input[i].UUID) {
		
			this.response.sendError(400, 2, 'Input validation error: ["members"]');
			return;
			
		}
	
	}
	
	// Find the specified group
	this.find('group', uuid, this, function(group) {

        //check if user has rights to remove members from group
        if(!session.admin && group.owner !== me &&
            (!group.organizerIds ||
                group.organizerIds.indexOf(session.id_user) < 0)) {
			this.response.sendError(401, 7, 'Admin authorization required to call this function.');
			return;
		}
		
		var errors = [];
		
		// Get the organizer ids
		var organizers = (group.organizerIds instanceof Array) ?
			group.organizerIds : [];
		
		// Get the member uuids as an array
		var members = (group.memberIds instanceof Array) ?
			group.memberIds : [];
			
		// Determine the user permission
		var canRemoveOrganizers = (session.admin || group.owner === me);
		
		for (var i in input) {
		
			var uuid = input[i].UUID;
			
			// Make sure the user is a member of this group
			var index = members.indexOf(uuid);
			
			if (index < 0) {
			
				// The specified user is not a member of this group
				errors.push(uuid);
				continue;
				
			}

            // Remove an organizer from this group.
            if ((index = organizers.indexOf(uuid)) > -1) {

                if (!canRemoveOrganizers) {

                    this.response.sendError(401, 19, 'Insufficient privileges to remove organizer from group.');
                    return;

                }

                organizers.splice(index, 1);
            }

			// Remove the member from this group
			members.splice(index, 1);

		
		}
		
		// Report any encountered errors
		if (errors.length > 0) {
		
			this.response.sendError(404, 18, 'User was not a group member: ["' + errors.join('", "') + '"]');
			return;
			
		}
		
		// Update the group
		group.memberIds = members;
		group.organizerIds = organizers;
		
		this.couchDB.store(group, this, function(success, id, revision) {
		
			if(success) {
				this.response.sendOK();
			} else {
				this.response.sendError(500, -1,
					'Unable to post to CouchDB.'
				);
			}
		
		});
	
	});

});

/**
 * Adds the current user to an existing group.
 */
$.app.handle('POST', '/group/%w/joinGroup', ['smile-session'], function(uuid) {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	var input = this.request.body;
	
	// Extract the passcode from the request, if any
	var passcode = (input) ? input.passcode : undefined;

	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		// Get current members as an array
		var members = (record.memberIds instanceof Array) ?
			record.memberIds : [];
		var membersIndex = members.indexOf(session.id_user);
		
		// You cannot join this group as you are already a member of this group.
		if(membersIndex > -1 || record.owner == session.id_user) {
			this.response.sendError(400, 21,
				'You are already a member of this group.'
			);
			
			return;
		}
		
		// Get invited members as an array
		var invited = (record.invitedIds instanceof Array) ?
			record.invitedIds : [];
		var invitedIndex = invited.indexOf(session.id_user);
		
		// Make sure the user can join the group
		if(
			
			// Not in invite list, for a private group
			(invitedIndex < 0) && 
			
			// Public with passcode validation
			(record['public'] && record.requirePasscode && record.passcode !== passcode) ){
		
			this.response.sendError(403, -1,
				record['public'] ?
					'Incorrect passcode given.' : 
					'You can not join without an invite.'
			);
			
			return;
		}
		
		// Remove the user from the inviteIds if he exists
		if(invitedIndex > -1) {
			invited.splice(invitedIndex, 1);
		}
		
		// Add the user to the members array
		members.push(session.id_user);
		
		// Update the record and store it
		record.invitedIds = invited;
		record.memberIds = members;
		
		this.couchDB.store(record, this, function(success, id, revision) {
					
			if(success) {
				this.response.sendOK();
			} else {
				this.response.sendError(500, -1,
					'Unable to post to CouchDB.'
				);
			}
		
		});
		
	});
});

/**
 * Adds organizers to an existing group.
 */
$.app.handle('PUT', '/group/%w/addOrganizers', ['smile-session', 'smile-input'], function(uuid) {
		

	// Get the current session and input data
	var session = this.session.data;
	var me = session.id_user;
	var input = this.request.body;
	
	// Get the given members
	var organizers = input.organizers;
	
	if(!organizers || !(organizers instanceof Array)) {
	
		this.response.sendError(400, 1,
			'Required fields in request are missing: ["organizers"]'
		);
		
		return;
		
	}
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
	
		// Admin authorization required to add members to a group in which you 
		// are not an organizer.
		if(!session.admin && 
			record.owner !== session.id_user &&
			(!(record.organizerIds instanceof Array) || 
				record.organizerIds.indexOf(session.id_user) < 0)) {
			
			this.response.sendError(401, 7,
				'Admin authorization required to call this function.'
			);
			
			return;
				
		}
		
		// Go through all entries
		var length = organizers.length;
		for(var i = 0; i < length; ++i) {
		
			var organizer = organizers[i];
			var muid = organizer.UUID;
			
			if(!muid) {
				
				this.response.sendError(400, 1,
					'Required fields in request are missing: ["organizers[' + i + '][UUID]"]'
				);
		
				return;
				
			}
			
			// Modify the members and organizers array
			var cmembers = (record.memberIds instanceof Array) ?
				record.memberIds : [];
			
			var corganizers = (record.organizerIds instanceof Array) ?
				record.organizerIds : [];
			
			// Add the member new member id
			if(cmembers.indexOf(muid) < 0) {
				cmembers.push(muid);
			}
			
			// Add him as an organizer as well
			if(corganizers.indexOf(muid) < 0) {
				corganizers.push(muid);
			}
			
			record.memberIds = cmembers;
			record.organizerIds = corganizers;
		
		}
		
		// Save changes
		if(length > 0) {
		
			this.couchDB.store(record, this, function(success) {
			
				if(success) {
					this.response.sendOK();
				} else {
					this.response.sendError(500, -1,
						'Failed to post to CouchDB.'
					);
				}
			
			});
		
		} else {
			this.response.sendOK();
		}
		
	});

});

/**
 * Removes organizers to an existing group.
 */
$.app.handle('PUT', '/group/%w/removeOrganizers', ['smile-session', 'smile-input'], function(uuid) {
		

	// Get the current session and input data
	var session = this.session.data;
	var me = session.id_user;
	var input = this.request.body;
	
	// Get the given members
	var organizers = input.organizers;

	if(!organizers || !(organizers instanceof Array)) {
	
		this.response.sendError(400, 1,
			'Required fields in request are missing: ["organizers"]'
		);
		
		return;
		
	}
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		// Get the existing record.
		var corganizers = (record.organizerIds instanceof Array) ?
			record.organizerIds : [];

		// Get the user status regarding this group
		var isGroupOwner = record.owner === session.id_user;
		var isGroupOrganizer = corganizers.indexOf(session.id_user) > -1
		
		// Admin authorization required to remove members to a group in 
		// which you are not an organizer.
		if(!session.admin && !isGroupOwner && !isGroupOrganizer) {
			
			this.response.sendError(401, 7,
				'Admin authorization required to call this function.'
			);
			
			return;
				
		}

		// Go through all entries
		var length = organizers.length;
		for(var i = 0; i < length; ++i) {
		
			var organizer = organizers[i];
			var muid = organizer.UUID;
			
			if(!muid) {
				
				this.response.sendError(400, 1,
					'Required fields in request are missing: ["organizers[' + i + '][UUID]"]'
				);
		
				return;
				
			}
			
			// Cannot remove owner from group.
			if(muid === record.owner) {
				this.response.sendError(401, 22,
					'Cannot remove owner from group organizers.'
				);
				
				return;
			}
			
			var index;
			
			// Remove the organizer
			if((index = corganizers.indexOf(muid)) > -1) {		
				corganizers.splice(index, 1);
				
			} else {
			
				this.response.sendError(404, 18,
					'User was not a group organizer ["' + muid + '"]'
				);
				
				return;
				
			}			
		
		}
		
		// Save changes
		if(length > 0) {
		
			record.organizerIds = corganizers;
			
			console.log('Group: posting to couchdb: ');
			console.log(record);
		
			this.couchDB.store(record, this, function(success, id, revision) {
			
				if(success) {
					this.response.sendOK();
				} else {
					this.response.sendError(500, -1,
						'Failed to post to CouchDB.'
					);
				}
			
			});
		
		} else {
			this.response.sendOK();
		}
		
	});

});

/**
 * Changes the owner of an existing group.
 */
$.app.handle('PUT', '/group/%w/changeOwner', ['smile-session', 'smile-input'], function(uuid) {

	// Get the current session and input data
	var session = this.session.data;
	var me = session.id_user;
	var input = this.request.body;
	
	// Make sure input was given
	var owner = input.owner;
	
	if(!owner) {
		this.response.sendError(400, 1,
			'Missing required fields in request: ["owner"]'
		);
		
		return;
	}
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		
		// Admin authorization required to change the owner for a group in which
		// you are not the owner.
		if(record.owner !== session.id_user && !session.admin) {
		
			this.response.sendError(401, 7,
				'Admin authorization required to call this function.'
			);
			
			return;
		
		}
		
		// Find the user with that owner id
		this.find('user', owner, this, function() {

            //set new owner
            record.owner = owner;

            //make sure to add new owner to organizerIds
            record.organizerIds = _.union(record.organizerIds, [ owner ]);

			// Save changes to the group
			this.couchDB.store(record, this, function(success) {
			
				if(success) {
					this.response.sendOK();
				} else {
					this.response.sendError(500, -1, 'Unable to post to couchdb.');
				}
			
			});
		
		});
		
	});
});

/**
 * Fetches invites for the current user
 */
$.app.handle('GET', '/group/fetchInvites', ['smile-session'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the record by name
	this.couchDB.get('group', 'by_invitee', { key: me }, this, function(records) {
		
		var results = [];
	
		// Go through all records
		for(var i in records) {
			var record = records[i];			
			
			results.push({
				name: record.name,
				UUID: record._id
			});
		}
		
		this.response.send(200, {
			success: true,
			groupsInvitedTo: results
		});
	
	});

});

/**
 * Fetches the groups of the current user
 */
$.app.handle('GET', '/group/fetchGroups', ['smile-session'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Own user and admins can see all groups (public and private)
	var keys = [[me, true], [me, false]];
	
	// Find the record by name
	this.couchDB.get('group', 'by_member', { keys: keys }, this, function(records) {
	
		var results = [];
        var UUIDs = {};
		
		// Go through all records
		for(var i in records) {
			var record = records[i];

            //TODO: Check for CouchDB alternative implementation using reduce. See: http://guide.couchdb.org/draft/views.html
            //don't add duplicate records
            if (!UUIDs[record._id]) {
                results.push({
                    name: record.name,
                    UUID: record._id
                });
                UUIDs[record._id] = true;
            }
		}

		this.response.send(200, {
			success: true,
			groups: results
		});
	
	});

});

/**
 * Fetches the groups of the current user
 */
$.app.handle('GET', '/group/fetchGroups/%w', ['smile-session'], function(uuid) {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	this.find('user', uuid, this, function(user) {
	
		// Own user and admins can see all groups (public and private)
		var keys = [[uuid, true]];
	
		if (session.admin || uuid === me) {
			keys.push([uuid, false]);
		}
	
		// Find the record by name
		this.couchDB.get('group', 'by_member', { keys: keys }, this, function(records) {
	
			var results = [];
            var UUIDs = {};
		
			// Go through all records
			for(var i in records) {
				var record = records[i];

                //TODO: Check for CouchDB alternative implementation using reduce. See: http://guide.couchdb.org/draft/views.html
                //don't add duplicate records
                if (!UUIDs[record._id]) {
                    results.push({
                        name: record.name,
                        UUID: record._id
                    });
                    UUIDs[record._id] = true;
                }
			}
		
			this.response.send(200, {
				success: true,
				groups: results
			});
	
		});
	
	});

});

/**
 * Creates a new group.
 */
$.app.handle('POST', '/group', ['smile-session'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the input data
	var input = this.request.body;
	
	// Validate the user input data
	$.schemaGroup.validator.validate('POST', input, 
			this, function(report, input) {
	
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, input);
			return;
			
		}

		if(!session.admin) {
		
			// Admin authorization required to create a group with a 
			// different owner than the currently authenticated user.			
			if(input.owner !== session.id_user) {
				this.response.sendError(401, 7, 'Admin authorization required to call this function.');
				return;
			}
		
		}
		
		// Build the final document and post it to couchdb
		var document = {};
		$.object.extend(document, $.schemaGroup.template, input);

        console.log($);

        //Add owner to organizersId
        if(!document.organizerIds) document.organizerIds = [];
        document.organizerIds = _.union(document.organizerIds, document.owner);

        //Add organizer IDs memberIds array
        document.memberIds = _.union(document.organizerIds, document.memberIds);

        //Remove from inivitedIds any users already in group
        document.invitedIds = _.difference(document.invitedIds, document.memberIds);

		// Insert the created timestamp
		var now = new Date();
		document.createdOn = [
			now.getFullYear(), 
			now.getMonth()+1, 
			now.getDate(),
			now.getHours(),
			now.getMinutes(),
			now.getSeconds()
		];
		
		// Store the document in CouchDB
		this.couchDB.store(document, this, function(success, id, revision) {
		
			if(success) {
			
				this.response.sendUUID(id);
					
				// Add the group photo as an attachment of this document
				var photo = this.request.files['photo'];
				
				if(photo) {
					this.setAttachment(id, revision, photo, 'photo');
				}
				
			} else {
			
				$.error('GroupModule: failed to store document in couchdb.');
				this.response.sendError(500, -1, 'Internal server error.');
			
			}
		
		});
			
	});
});

/**
 * Updates an existing group.
 */
$.app.handle('PUT', '/group/%w', ['smile-session'], function(uuid) {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the input data
	var input = this.filter(this.request.body);
	input._id = uuid;
	
	// Validate the user input data
	$.schemaGroup.validator.validate('PUT', input, 
			this, function(report, input) {
	
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, input);
			return;
			
		}
		
		// Find the old record
		this.find('group', uuid, this, function(record) {
			
			if(!session.admin) {
			
				// Admin is required to edit a group you are not an owner or 
				// organizer of
				if(!session.admin && record.owner !== session.id_user && 
						(!record.organizerIds || 
							record.organizerIds.indexOf(session.id_user) < 0)) {
						
					this.response.sendError(401, 7, 'Admin authorization required to call this function.');
					return;
				}
			
				// You cannot change the owner of a group that you do not own 
				// unless you are an admin.
				if(input.owner && record.owner !== input.owner) {
					
					this.response.sendError(401, 17,
						'Admin authorization required to change the owner for a group that you do not own.'
					);
					
					return;
					
				}
				
			}
		
			// Build the final document and post it to couchdb
			var document = {};
			$.object.extend(document, $.schemaGroup.template, record, input);

            //Ensure owner is part of organizersId
            if(!document.organizerIds) document.organizerIds = [];
            document.organizerIds = _.union(document.organizerIds, document.owner);

            //Ensure organizer IDs are included in memberIds array
            document.memberIds = _.union(document.organizerIds, document.memberIds);

            //Remove from inivitedIds any users already in group
            document.invitedIds = _.difference(document.invitedIds, document.memberIds);

			// Store the document in CouchDB
			this.couchDB.store(document, this, function(success, id, revision) {
		
				if(success) {
			
					this.response.sendOK();
					
					// Add the group photo as an attachment of this document
					var photo = this.request.files['photo'];
				
					if(photo) {
						this.setAttachment(id, revision, photo, 'photo');
					}
				
				} else {
			
					$.error('GroupModule: failed to store document in couchdb.');
					this.response.sendError(500, -1, 'Internal server error.');
			
				}
		
			});
		
		});
			
	});
});

/**
 * Deletes an existing group.
 */
$.app.handle('DELETE', '/group/%w', ['smile-session'], function(uuid) {
	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		// Admin authorization required to delete a group created by a 
		// different user.
		if(record.owner !== me && !session.admin) {
			
			this.response.sendError(401, 7,
				'Admin authorization required to call this function.'
			);
			
			return;
			
		}
		
		// Delete the document from CouchDB
		this.couchDB.delete(record, this, function(success) {
		
			if(success) {
				this.response.sendOK();
			} else {
				this.response.sendError(500, -1, 'Unable to delete from CouchDB.');
			}
		
		});
	
	});
});

/**
 * Fetches an existing group.
 */
$.app.handle('GET', '/group/%w', ['smile-session'], function(uuid) {
	
	// Find the old record
	this.find('group', uuid, this, function(record) {
		
		this.response.send(200, {
			success: true,
			group: sanitize_group_document.call(this, record)
		});
		
	});
	
});
