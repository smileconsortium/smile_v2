/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaActivity',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'creator,photo'],
		
		// Required
		['required', 'name,activityType', 'POST'],
		
		// Boolean
		['bool', 'public'],
		
		// Arrays
		['string-array', 'tags,gradeLevels,subjects']
		
	
	]),
	
	// Document template
	template: {
		doctype: 'activity',
		
		name: undefined,
		description: undefined,
		activityType: undefined,
		owner: undefined,
		owningGroup: undefined,
		institution: undefined,
		createdOn: undefined,
		"public": true,
		tags: undefined,
		gradeLevels: undefined,
		subjects: undefined,
		language: "English"
	}

};

