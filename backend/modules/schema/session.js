/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaSession',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'creator,photo'],
		
		// Required
		['required', 'activity,group'],
		
		// Boolean
		['bool', 'visible,timed,requireRatings,enableRatings,enableComments,hideUserDetails,showCorrect,showResults']
		
	
	]),
	
	// Document template
	template: {
		doctype: 'session',
		
		name: undefined,
		description: undefined,
		activity: undefined,
		group: undefined,
		status: "closed",
		showResults: true,
		showCorrect: true,
		hideUserDetails: false,
		enableComments: true,
		enableRatings: true,
		requireRatings: false,
		visible: false,
		timed: false,
		timerSecs: 0,
		language: "English"
	}

};

