var nodemailer = require('nodemailer');

var mailOpts, smtpConfig;
smtpConfig = nodemailer.createTransport('SMTP', {
    host: 'smtp-unencrypted.stanford.edu', // required
    port: 25, // optional, defaults to 25 or 465
    use_authentication: false, // optional, false by default
    user: '', // used only when use_authentication is true
    pass: ''  // used only when use_authentication is true
});

//construct the email sending module
mailOpts = {
//from: req.body.name + ' &lt;' + req.body.email + '&gt;',
    from: '',
    to: '',
//replace it with id you want to send multiple must be separated by ,(comma)
    subject: 'SMILE message',
    text: ''
};

exports.send = function (from, to, subject, msg) {
    mailOpts.from = from;
    mailOpts.to = to;
    mailOpts.subject = subject;
    mailOpts.text = msg;

//send Email
    console.log('Sending email');
    smtpConfig.sendMail(mailOpts, function (error, response) {
//Email not sent
        if (error) {
            console.log("Email send Falied: " + error);
            return true;
        }
//email sent successfully
        else {
            console.log("Email sent successfully");
            return false;
        }
    });
};
