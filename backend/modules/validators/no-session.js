/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

/**
 * Requires the user to not be authenticated when making the request that's
 * currently being validated.
 *
 * @param HttpMessage request
 *	The instance of the request being validated.
 *
 * @param HttpResponse response
 *	The response associated with the request.
 *
 * @param HttpSession session
 *	The session associated with the request.
 */
$.app.setCustomValidator('smile-no-session', function(request, response, session) {

	if(session.data.authenticated) {
		response.sendError(403, -1, 'You must be a guest in order to call this function.');
		return false;
	}
	
	return true;

});