$.ajax({

	url: '/group',
	type: 'POST',
	dataType: 'json',
	contentType: 'application/json', 
	data: JSON.stringify({
		name: "Neticle Portugal",
		groupType: "test",
		owner: "THIS.IS.AN.INVALID.UUID",
		'public': true,
		requirePasscode: true,
		passcode: "smile@neticle",
		tags: ["science", "grade:12", "chemistry", "AP chemistry", "AP chem", "atomic chemistry", "orbitals"],
		gradeLevels: ["12"],
		subjects: ["chemistry", "science", "physical sciences"],
		language: "English"
	})
});


/*

POST /group HTTP/1.1
Host: localhost:1337
Connection: keep-alive
Content-Length: 338
Accept: application/json, text/javascript, *\/*; q=0.01
Origin: http://localhost:1337
X-Requested-With: XMLHttpRequest
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36
Content-Type: application/json
Referer: http://localhost:1337/
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8
Cookie: session=954aca9c365b29f05605b8c9db10e205631ca83f

{"name":"Neticle Portugal","groupType":"test","owner":"0cbc6dc62a8f09f1b7b6a51885003164","public":true,"requirePasscode":true,"passcode":"smile@neticle","tags":["science","grade:12","chemistry","AP chemistry","AP chem","atomic chemistry","orbitals"],"gradeLevels":["12"],"subjects":["chemistry","science","physical sciences"],"language":"English"}



HTTP/1.1 200 OK
Set-Cookie: session=f0ee42c7f258f6ba48c6d7e4e1491f62b2a1d573; domain=; path=/
Content-Type: application/json
Transfer-Encoding: chunked
Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: origin, content-type, accept
Connection: keep-alive

{"success":true,"UUID":"0cbc6dc62a8f09f1b7b6a518850038dd"}

*/
