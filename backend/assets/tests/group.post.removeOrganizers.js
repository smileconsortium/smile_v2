$.ajax({

	url: '/group/0cbc6dc62a8f09f1b7b6a518850038dd/removeOrganizers',
	type: 'PUT',
	dataType: 'json',
	contentType: 'application/json',
	data: JSON.stringify({
		organizers: [
			{ UUID: '0cbc6dc62a8f09f1b7b6a518850016db' }
		]
	 })
	
});

/*

PUT /group/0cbc6dc62a8f09f1b7b6a518850038dd/removeOrganizers HTTP/1.1
Host: localhost:1337
Connection: keep-alive
Content-Length: 60
Accept: application/json, text/javascript, *\/*; q=0.01
Origin: http://localhost:1337
X-Requested-With: XMLHttpRequest
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36
Content-Type: application/json
Referer: http://localhost:1337/
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8
Cookie: session=5df15279f5f6dba66616756e7a0cc93eb7bd263b


HTTP/1.1 200 OK
Set-Cookie: session=ebf8a8bbe8a29080d1f787dc7074d83d80e9db21; domain=; path=/
Content-Type: application/json
Transfer-Encoding: chunked
Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: origin, content-type, accept
Connection: keep-alive

{"success":true}

*/
