/* by id */
function(doc) {
	if(doc.doctype === 'institution') {
		emit(doc._id, doc);
	}
}

/* by name */
function(doc) {
	if(doc.doctype === 'institution') {
		emit(doc.name, doc);
	}
}
