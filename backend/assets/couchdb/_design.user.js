/* user.by_id */
function(d) {

	if(d.doctype === 'user') {
		emit(d._id, d);
	}

}

/* user.by_username */
function(d) {

	if(d.doctype === 'user') {
		emit(d.username, d);
	}
	
}

/* user.by_emailAddresses */
function(d) {

	// Only emit users with email
	if(d.doctype === 'user' && d.emailAddresses) {
	
		// Go through all email addresses
		var collection = d.emailAddresses;
		for(var i = 0, length = collection.length; i < length; ++i) {
			emit(collection[i], d);
		}
		
	}
	
}

/* user.by_username_password */
function(d) {

	if(d.doctype === 'user') {
		emit([d.username, d.password], d);
	}

}

/* user.by_email_password */
function(d) {

	// Only emit users with email
	if(d.doctype === 'user' && d.emailAddresses) {
	
		// Go through all email addresses
		var collection = d.emailAddresses;
		for(var i = 0, length = collection.length; i < length; ++i) {
			emit([collection[i], d.password], d);
		}
		
	}
	
}
