/* by_id */
function(d) {
	if(d.doctype === 'activity') {
		emit(d._id, d);
	}
}

/* by_owner */
function(d) {
	if(d.doctype === 'activity' && d.owner) {
		emit(d.owner, d);
	}
}

/* by_owningGroup */
function(d) {
	if(d.doctype === 'activity' && d.owningGroup) {
		emit(d.owningGroup, d);
	}
}

