window.jQuery(function () {

    var _ = window._;
    //var app = window.app;
    var test = window.test;
    var equal = window.equal;

    var app = window.app;
    //test in production mode
    app.developmentMode = false;

    module('smilem');

    test('app container exists', function () {
        var appId = $("#app-container").attr("id");
        equal(appId, "app-container");
    });

    test('app obj exists', function () {
        equal(typeof(app), "object");
    });


});
