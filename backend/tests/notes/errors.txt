Errors:
==========================================================
- GET /institution/getByName/{name}
    - Doesn't find the matching institutions when the name contains spaces. Institution names should be allowed to contain spaces and other HTML-escaped characters.

==========================================================
- POST /institution
    - Allows for institutions with same name

==========================================================
- GET /group/getByName/{name}
    - Doesn't find the matching group when the name contains spaces. Group names should be allowed to contain spaces and other HTML-escaped characters.

==========================================================


Unimplemented Functions:
==========================================================
- GET /institution/nameAutoFill
    - Not implemented

==========================================================
- GET /group/searchGroups/
    - Not implemented

==========================================================




====================================================================================================================
====================================================================================================================
                                          Corrected Errors (incomplete list)
====================================================================================================================
====================================================================================================================



- PUT /user/{UUID}
    - Currently causes the node.js server to crash

==========================================================
- GET /user/{UUID}
    - Passing onlyFields paramater does not cause only those listed properties to be returned in the response. Full user object is still sent.

==========================================================
- GET /user/usernameIsUnique
    - Needs access without being signed in as a SMILE user

==========================================================
- GET /user/emailIsUnique
    - Needs access without being signed in as a SMILE user

==========================================================
- POST /user/getUsernamesFromUUID/
    - Error code is 9 missing: 9   User(s) not found with UUID(s): [UUID, UUID]    One or more of the UUIDs did not match corresponing User objects in database.

==========================================================
- POST /user/batchAdd/
    - Does not work. No users are not created

==========================================================
- PUT /group/{group}
    - Does not update invitedIds array. No invitedIds appear after call, although other fields update

==========================================================
- DELETE /group/{UUID}
    - Causes the server to hang, does not delete group

==========================================================
- PUT /group/{UUID}/addMember
    - Returns input validation error

==========================================================
- PUT /group/{UUID}/removeMembers
    - Causes the server to crash

==========================================================
- POST /group/{group}/joinGroup/
    - Non-invited users can join.

==========================================================
- PUT /group/{UUID}/changeOwner
    - Causes the server to crash

==========================================================
- GET /group/fetchGroups
    - Causes the server to crash

==========================================================
- GET /group/fetchGroups/{user}
    - Does not fetch groups, unspecified error message given in response:
        - {"status":404,"description":"Document Not Found"}

==========================================================
- GET /institution/getByName/{institution}
    - Does not return, hangs indefinitely

==========================================================
- POST /activity
    - Crashes the server

==========================================================
- PUT /activity
    - Crashes the server, error code response does not return 

==========================================================