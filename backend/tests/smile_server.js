//requires that user0 already exists and has admin=true
var server = 'http://localhost:8080/smileService/';
//var server = 'http://171.64.185.248/5500/';

var state;

var usePreviousState = false;
var previousState = '{"loggedInAs":{"firstName":"User","lastName":"Five","username":"user5","email":"test5@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7cd64"},"user0":{"firstName":"User","lastName":"Zero","username":"user0","email":"servicesTest0@test.net","password":"testPassword9901","UUID":"aa8992d2799cfb58ebc1e8f165000816"},"user1":{"firstName":"User","lastName":"One","username":"user1","email":"test1@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7aa1c"},"user2":{"firstName":"User","lastName":"Two","username":"user2","email":"test2@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7b87e"},"user3":{"firstName":"User","lastName":"Three","username":"user3","email":"test3@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7ba80"},"user4":{"firstName":"User","lastName":"Four","username":"user4","email":"test4@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7c67c"},"user5":{"firstName":"User","lastName":"Five","username":"user5","email":"test5@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7cd64"},"user6":{"firstName":"User","lastName":"Six","username":"user6","email":"test6@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7d3df"},"user7":{"firstName":"User","lastName":"Seven","username":"user7","email":"test7@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7d8d2"},"user8":{"firstName":"User","lastName":"Eight","username":"user8","email":"test8@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7e205"},"user9":{"firstName":"User","lastName":"Nine","username":"user9","email":"test9@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7eb91"},"user10":{"firstName":"User","lastName":"Ten","username":"user10","email":"test10@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7f503"},"user11":{"firstName":"User","lastName":"Eleven","username":"user11","email":"test11@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7ff51"},"photoUser":{"username":"photoUser","email":"picassa@photo.org","password":"iLikePhotos89"},"previousResult":{"success":true},"institution0":{"name":"SMILEConsortium","institutionType":"Non-profit","country":"United States","region":"California","city":"Los Altos","website":"www.smileconsortium.org","UUID":"468b5e1b412e8fa1292bfeecd0e80b54"},"institution1":{"name":"Gotham","institutionType":"Government","country":"United States","region":"New York","UUID":"468b5e1b412e8fa1292bfeecd0e80e37"},"group0":{"UUID":"468b5e1b412e8fa1292bfeecd0e819c2","memberIds":["468b5e1b412e8fa1292bfeecd0e7cd64","468b5e1b412e8fa1292bfeecd0e7b87e","468b5e1b412e8fa1292bfeecd0e7ba80","468b5e1b412e8fa1292bfeecd0e7c67c","468b5e1b412e8fa1292bfeecd0e7d3df","468b5e1b412e8fa1292bfeecd0e7aa1c","468b5e1b412e8fa1292bfeecd0e7e205","468b5e1b412e8fa1292bfeecd0e7d8d2"],"organizerIds":["468b5e1b412e8fa1292bfeecd0e7cd64","468b5e1b412e8fa1292bfeecd0e7b87e"],"public":true,"requirePasscode":true,"language":"English","name":"EdTech","groupType":"standard","institution":"12341234","owner":"468b5e1b412e8fa1292bfeecd0e7b87e","passcode":"smileLearning","tags":["science","grade:12","chemistry","AP chemistry","AP chem","atomic chemistry","orbitals"],"gradeLevels":["9","12"],"subjects":["chemistry","science","physical sciences"],"invitedIds":[],"createdOn":[2014,1,4,15,40,33]},"group1":{"name":"AlgebraMrRyan","UUID":"468b5e1b412e8fa1292bfeecd0e8185f","memberIds":["468b5e1b412e8fa1292bfeecd0e7d8d2","468b5e1b412e8fa1292bfeecd0e7cd64","468b5e1b412e8fa1292bfeecd0e7e205","468b5e1b412e8fa1292bfeecd0e7eb91","468b5e1b412e8fa1292bfeecd0e7f503"],"organizerIds":["468b5e1b412e8fa1292bfeecd0e7d8d2"]},"activity0":{"name":"Intro Quiz","description":"","activityType":"flippedMC","owner":"468b5e1b412e8fa1292bfeecd0e7b87e","owningGroup":"468b5e1b412e8fa1292bfeecd0e8185f","institution":"468b5e1b412e8fa1292bfeecd0e80b54","createdOn":[2013,12,2,0,37,0],"public":true,"tags":["science","grade:12","chemistry","AP chemistry","AP chem","atomic chemistry","orbitals"],"gradeLevels":["12"],"subjects":["chemistry","science","physical sciences"],"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e82146"},"activity1":{"name":"Properties of Atoms","description":"","activityType":"exam","owner":"468b5e1b412e8fa1292bfeecd0e7eb91","owningGroup":"468b5e1b412e8fa1292bfeecd0e819c2","institution":"468b5e1b412e8fa1292bfeecd0e80b54","createdOn":[2013,12,2,0,40,42],"public":true,"UUID":"468b5e1b412e8fa1292bfeecd0e823c5"},"activity2":{"UUID":"468b5e1b412e8fa1292bfeecd0e82a7c"},"activity3":{"UUID":"468b5e1b412e8fa1292bfeecd0e837ce"},"activity4":{"UUID":"468b5e1b412e8fa1292bfeecd0e844f1"},"session0":{"name":"First math quiz","description":"The initial responses to the activity","activity":"468b5e1b412e8fa1292bfeecd0e82146","group":"468b5e1b412e8fa1292bfeecd0e819c2","status":"closed","showResults":true,"showCorrect":true,"hideUserDetails":false,"enableComments":true,"enableRatings":true,"requireRatings":false,"visible":false,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e85422"},"session1":{"activity":"468b5e1b412e8fa1292bfeecd0e82146","group":"468b5e1b412e8fa1292bfeecd0e8185f","UUID":"468b5e1b412e8fa1292bfeecd0e85ef1"},"session2":{"activity":"468b5e1b412e8fa1292bfeecd0e82146","group":"468b5e1b412e8fa1292bfeecd0e8185f","visible":true,"UUID":"468b5e1b412e8fa1292bfeecd0e86692"},"session3":{"activity":"468b5e1b412e8fa1292bfeecd0e823c5","group":"468b5e1b412e8fa1292bfeecd0e819c2","UUID":"468b5e1b412e8fa1292bfeecd0e8723b"},"session4":{"activity":"468b5e1b412e8fa1292bfeecd0e82146","group":"468b5e1b412e8fa1292bfeecd0e819c2","UUID":"468b5e1b412e8fa1292bfeecd0e878ef"},"resource0":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":{"firstName":"User","lastName":"Three","username":"user3","email":"test3@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7ba80"},"allowMultipleResponses":false,"responseLimit":2,"resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8793e"},"resource1":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":{"firstName":"User","lastName":"Two","username":"user2","email":"test2@test.net","password":"testPassword9901","UUID":"468b5e1b412e8fa1292bfeecd0e7b87e"},"allowCreatorResponse":false,"multiSelect":false,"selectLimit":3,"resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"allowMultipleResponses":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e884e4"},"resource2":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":"468b5e1b412e8fa1292bfeecd0e7eb91","allowResponseRevision":false,"resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e88788"},"resource3":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":"468b5e1b412e8fa1292bfeecd0e7aa1c","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e893b7"},"resource4":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":"468b5e1b412e8fa1292bfeecd0e7aa1c","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8969e"},"resource5":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":true,"creator":"468b5e1b412e8fa1292bfeecd0e7aa1c","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e89e92"},"resource6":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7b87e","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8a592"},"resource7":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7ba80","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8ac2f"},"resource8":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7c67c","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8b243"},"resource9":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7cd64","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8b50b"},"resource10":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7d8d2","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8bb68"},"resource11":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7d8d2","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8c51f"},"resource12":{"activityIds":["468b5e1b412e8fa1292bfeecd0e82146"],"public":false,"creator":"468b5e1b412e8fa1292bfeecd0e7d8d2","resourceType":"flippedQuestion","title":"What is 3 x 5?","description":"","answerChoices":[{"pos":3,"text":"5","correct":false},{"pos":0,"text":"10","correct":false},{"pos":1,"text":"15","correct":true,"response":"Good job!"},{"pos":2,"text":"6","correct":false}],"multiSelect":false,"allowMultipleResponses":false,"allowCreatorResponse":false,"allowResponseRevision":false,"visible":true,"timed":false,"timerSecs":60,"language":"English","UUID":"468b5e1b412e8fa1292bfeecd0e8d4ae"},"response0":{"userId":"468b5e1b412e8fa1292bfeecd0e7ba80","resourceId":"468b5e1b412e8fa1292bfeecd0e8793e","sessionId":"468b5e1b412e8fa1292bfeecd0e85422","answerChoice":[2],"secondsToAnswer":12.3,"rating":3.2},"response1":{"userId":"468b5e1b412e8fa1292bfeecd0e7c67c","resourceId":"468b5e1b412e8fa1292bfeecd0e8793e","sessionId":"468b5e1b412e8fa1292bfeecd0e85422","answerChoice":[1],"secondsToAnswer":11,"rating":4},"response2":{"userId":"468b5e1b412e8fa1292bfeecd0e7cd64","resourceId":"468b5e1b412e8fa1292bfeecd0e8793e","sessionId":"468b5e1b412e8fa1292bfeecd0e85422","answerChoice":[3],"secondsToAnswer":10,"rating":5},"response3":{"userId":"468b5e1b412e8fa1292bfeecd0e7c67c","resourceId":"468b5e1b412e8fa1292bfeecd0e884e4","sessionId":"468b5e1b412e8fa1292bfeecd0e85422","answerChoice":[0],"secondsToAnswer":4,"rating":3},"response4":{"userId":"468b5e1b412e8fa1292bfeecd0e7d3df","resourceId":"468b5e1b412e8fa1292bfeecd0e884e4","sessionId":"468b5e1b412e8fa1292bfeecd0e85422","answerChoice":[2],"secondsToAnswer":5,"rating":2},"response5":{"userId":"468b5e1b412e8fa1292bfeecd0e7d3df","resourceId":"468b5e1b412e8fa1292bfeecd0e884e4","sessionId":"468b5e1b412e8fa1292bfeecd0e8723b","answerChoice":[2],"secondsToAnswer":5,"rating":2}} ';

var tests = [ "userTests", "institutionTests", "groupTests", "activityTests", "sessionTests", "resourceTests", "responseTests" ];

//TEMP FOR TESTING, use previousState, don't re-run all tests
//tests = [ "responseTests" ];

//Will delay calling the passed function until after any key is pressed, and will only call the function once.
$.waitForKeyPress = function(callback) {
    var once = _.once(callback);
    $(document).keydown(function() {
        once();
    });
}

window.jQuery(function () {
    module('smile_server');

    if (usePreviousState) {
        state = JSON.parse(previousState);
        state.loggedInAs = null;
    } else {
        state = {};
        state.loggedInAs = null;
        state.previousResult;
    }

    /* TEST FUNCTIONS: servicesTest(name, url, type, user, data, expectedResult, actualResult, callback) */

    callTests();

    function callTests() {

        function test_recursive(num) {
            var test = window[tests[num]];
            if (num + 1 < tests.length) {
                test(function () {
                    test_recursive(num + 1);
                });
            } else {
                test();
            }
        }

        test_recursive(0);
    }
});


//UTILS

function getPropByString(obj, propString) {

    if (!propString) return obj;

    var prop, props = propString.split('.');

    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
        prop = props[i];

        if (typeof obj == 'object' && obj !== null && prop in obj) {
            obj = obj[prop];
        } else {
            break;
        }
    }
    return obj[props[i]];
}

function objToString(obj) {
    var results = _.pairs(obj);
    var string = "";
    for (var i = results.length; i > 0; i--) {
        var name = results[i - 1][0];
        var val = results[i - 1][1];
        if (typeof(val) == "object") {
            val = objToString(val);
            string += name + ": {" + val + "}";
        } else {
            string += name + ": " + val;
        }
        if (i > 1) string += ", ";
    }
    return string;
}
function servicesTest(name, url, type, user, data, expectedResult, actualResult, callback) {
    //first log in if a different user is provided
    if (user != null && user.username != state.loggedInAs) {
        if (user == "") {
            //logout if "" is provided as user and not logged out already
            logout(doAsyncTest);
        } else {
            //if a new user
            //log out if logged in
            if (state.loggedInAs && state.loggedInAs.length > 0) {
                logout(function () {
                    login(user, doAsyncTest);
                });
            } else {
                login(user, doAsyncTest);
            }
        }
    } else {
        doAsyncTest();
    }
    function doAsyncTest() {
        asyncTest(name, function () {
            var aj = {
                url: server + url,
                type: type,
                complete: function (result, status) {
                    start();
                    var response = { error: "No response received from server." };
                    if (result) {
                        response = JSON.parse(result.responseText);
                    }
                    var string = "{" + objToString(response) + "}";
                    if (typeof(expectedResult) != undefined && typeof(actualResult) != undefined) {
                        var actual = getPropByString(response, actualResult);
                        equal(actual, expectedResult, "Success - " + string
                            + ".\n Expected Result: " + expectedResult
                            + ",\n Actual Result: " + actual);
                    } else {
                        equal(1, 1, "Success - " + string);
                    }
                    //console.log('servicesTest.doAsyncTest(' + name + ')');
                    state.previousResult = response;
                    //console.log(response);
                    if (callback) callback(response, string);
                }
            }
            if (type == 'POST' || type == 'PUT' || (type == 'DELETE' && data)) {
                aj.dataType = 'json';
                aj.contentType = 'application/json';
            }
            if (data) {
                aj.data = JSON.stringify(data);
            }
            $.ajax(aj);
        });
    }
}
function logout(callback) {
    $.ajax({
        url: server + 'auth/logout',
        type: 'POST',
        success: function (result) {
            state.loggedInAs = null;
            callback();
        },
        error: function (result) {
            callback();
        }
    });
}
function login(user, callback) {
    if (_.isEqual(state.loggedInAs, user)) {
        response = {
            user: user
        };
        callback(response);
        return;
    }
    logout(function () {
        $.ajax({
            url: server + 'auth/login',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                username: user.username,
                password: user.password
            }),
            complete: function (result, status) {
                var response = JSON.parse(result.responseText);
                if (status == "success") {
                    state[user.username].UUID = JSON.parse(result.responseText).UUID;
                    //state[console];
                    state.loggedInAs = user;
                    //state.loggedInAs = user.username;
                }
                callback(response);
            }
        });
    });
}
function deleteUser(user, callback) {
    //login as user first
    if (!user.UUID) {
        login(state.user0, function (response) {
            $.ajax({
                url: server + 'user/getByUsername/' + user.username,
                type: 'GET',
                complete: function (result, status) {
                    var response = JSON.parse(result.responseText);
                    console.log(response);
                    if (response.user && response.user.UUID) user.UUID = response.user.UUID;
                    deleteByUUID(user);
                }
            });
        });
    } else {
        deleteByUUID(user);
    }
    function deleteByUUID(user) {
        login(state.user0, function (response) {
            //console.log(response);
            $.ajax({
                url: server + 'user/' + user.UUID,
                type: 'DELETE',
                complete: function (result, status) {
                    if (status == "success") {
                        console.log("successfully deleted user: " + user.username);
                    } else {
                        console.log("couldn't delete user: " + user.username);
                    }
                    if (callback) callback();
                }
            });
        });
    }

}
function deleteGroup(group, callback) {
    //get group name
    $.ajax({
        url: server + 'group/getByName/' + group.name,
        type: 'GET',
        complete: function (result, status) {
            if (status == "success") {
                var response = JSON.parse(result.responseText);
                var uuid = getPropByString(response, "group.UUID");
                //group exists, so need to delete
                login(state.user0, function () {
                    $.ajax({
                        url: server + 'group/' + uuid,
                        type: 'DELETE',
                        complete: function (result, status) {
                            if (status == "success") {
                                console.log("successfully deleted group: " + group.name);
                            } else {
                                console.log("couldn't delete user: " + group.name);
                            }
                            if (callback) callback();
                        }
                    });
                })
            } else {
                console.log("Group " + group.name + " doesn't exist.");
                if (callback) callback();
            }
        }
    });
}