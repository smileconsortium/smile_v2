//session0 - activity0, group0, visible
//session1 - activity0, group1, hidden
//session2 - activity0, group1, visible
//session3 - activity1, group0, hidden


window.sessionTests = function (callback) {
    state.session0 = {
        name: "First math quiz",
        description: "The initial responses to the activity",
        activity: state.activity0.UUID,
        group: state.group0.UUID,
        status: "closed",
        showResults: true,
        showCorrect: true,
        hideUserDetails: false,
        enableComments: true,
        enableRatings: true,
        requireRatings: false,
        visible: false,
        timed: false,
        timerSecs: 60,
        language: "English"
    }
    state.session1 = {
        activity: state.activity0.UUID,
        group: state.group1.UUID
    }
    state.session2 = {
        activity: state.activity0.UUID,
        group: state.group1.UUID,
        visible: true
    }
    state.session3 = {
        activity: state.activity1.UUID,
        group: state.group0.UUID
    }
    state.session4 = {
        activity: state.activity0.UUID,
        group: state.group0.UUID
    }

    createSession0();

    function createSession0() {
        var session = state.session0;
        servicesTest('create session0 - non-group member', 'session', 'POST', state.user9,
            session, false, "success",
            function (response) {
                equal(response.error && response.error.code, 27, "Correct error code returned");
                createSession1();
            }
        );
    }

    function createSession1() {
        var session = state.session0;
        servicesTest('create session0 - group member', 'session', 'POST', state.user7,
            session,
            false,
            "success",
            function (response) {
                equal(response.error && response.error.code, 27, "Correct error code returned");
                createSession2();
            }
        );
    }

    function createSession2() {
        var session = state.session0;
        servicesTest('create session0 - group organizer', 'session', 'POST', state.user5,
            session,
            true,
            "success",
            function (response) {
                if (response && response.UUID) session.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createSession3();
            }
        );
    }
    function createSession3() {
        var session = state.session1;
        servicesTest('create session1 - group owner', 'session', 'POST', state.user7,
            session,
            true,
            "success",
            function (response) {
                if (response && response.UUID) session.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createSession4();
            }
        );
    }
    function createSession4() {
        var session = state.session2;
        servicesTest('create session2 - admin', 'session', 'POST', state.user0,
            session,
            true,
            "success",
            function (response) {
                if (response && response.UUID) session.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createSession5();
            }
        );
    }
    function createSession5() {
        var session = state.session3;
        servicesTest('create session3 - admin', 'session', 'POST', state.user0,
            session,
            true,
            "success",
            function (response) {
                if (response && response.UUID) session.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createSession6();
            }
        );
    }
    function createSession6() {
        var session = state.session4;
        servicesTest('create session - admin', 'session', 'POST', state.user0,
            session,
            true,
            "success",
            function (response) {
                if (response && response.UUID) session.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                deleteSession0();
            }
        );
    }
    function deleteSession0() {
        var session = state.session4;
        servicesTest('delete session - non-group member', 'session/' + session.UUID, 'DELETE', state.user9,
            null,
            false,
            "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code.");
                deleteSession1();
            }
        );
    }
    function deleteSession1() {
        var session = state.session4;
        servicesTest('delete session - group member', 'session/' + session.UUID, 'DELETE', state.user4,
            null,
            false,
            "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code.");
                deleteSession2();
            }
        );
    }
    function deleteSession2() {
        var session = state.session4;
        servicesTest('delete session - group organizer', 'session/' + session.UUID, 'DELETE', state.user5,
            session,
            true,
            "success",
            function (response) {
                verifyDeletion();
            }
        );
    }

    function verifyDeletion() {
        var session = state.session4;
        servicesTest('verify session was deleted', 'session/' + session.UUID, 'GET', null, null, false, "success", function(response) {
            equal(response.success, false, "Deleted session not found.");
            equal(response.error && response.error.code, 13, "Correct GET /session error code returned for session not found.");
            getSession0();
        });
    }

    function getSession0() {
        var session = state.session0;
        servicesTest('get session0 - non-admin, non-group member', 'session/' + session.UUID, 'GET', state.user9,
            null, false, "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code returned");
                getSession1();
            }
        );
    }
    function getSession1() {
        var session = state.session0;
        servicesTest('get session0 - non-admin, group member', 'session/' + session.UUID, 'GET', state.user8,
            null, true, "success",
            function (response) {
                equal(session.UUID, response.session.UUID, "Correct UUID is fetched");
                equal(session.group, response.session.group, "Correct group UUID is fetched");
                equal(session.activity, response.session.activity, "Correct activity UUID is fetched");
                equal(session.description, response.session.description, "Correct description is fetched");
                updateSession0();
            }
        );
    }

    function updateSession0() {
        var session = state.session0;
        servicesTest('update session0, not session\'s group organizer', 'session/' + session.UUID, 'PUT', state.user4,
            {
                status: "create_questions",
                showResults: false,
                showCorrect: false,
                hideUserDetails: true,
                enableComments: true,
                enableRatings: true,
                requireRatings: true,
                visible: true,
                timed: false
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code returned");
                updateSession1();
            }
        );
    }
    function updateSession1() {
        var session = state.session0;
        servicesTest('update session0, session\'s group organizer, change activity', 'session/' + session.UUID, 'PUT', state.user5,
            {
                status: "create_questions",
                showResults: false,
                showCorrect: false,
                hideUserDetails: true,
                enableComments: true,
                enableRatings: true,
                requireRatings: true,
                visible: true,
                timed: false,
                activity: state.activity4.UUID
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 28, "Correct error code returned");
                updateSession2();
            }
        );
    }
    function updateSession2() {
        var session = state.session0;
        servicesTest('update session0, session\'s group organizer, change group', 'session/' + session.UUID, 'PUT', state.user5,
            {
                status: "create_questions",
                showResults: false,
                showCorrect: false,
                hideUserDetails: true,
                enableComments: true,
                enableRatings: true,
                requireRatings: true,
                visible: true,
                timed: false,
                group: state.group1.UUID
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 29, "Correct error code returned");
                updateSession3();
            }
        );
    }
    function updateSession3() {
        var session = state.session0;
        servicesTest('update session0, session\'s group organizer', 'session/' + session.UUID, 'PUT', state.user5,
            {
                status: "create_questions",
                showResults: false,
                showCorrect: false,
                hideUserDetails: true,
                enableComments: true,
                enableRatings: true,
                requireRatings: true,
                visible: true,
                timed: false
            }, true, "success",
            function (response) {
                getSession2();
            }
        );
    }
    function getSession2() {
        var session = state.session0;
        servicesTest('get session after update', 'session/' + session.UUID, 'GET', null,
            null, true, "success",
            function (response) {
                equal(session.UUID, response.session.UUID, "Correct UUID is fetched");
                equal(session.activity, response.session.activity, "Correct activity UUID is fetched");
                equal(session.group, response.session.group, "Correct group UUID is fetched");
                equal(response.session.status, "create_questions", "Correct status is fetched");
                equal(response.session.showResults, false, "Correct showResults is fetched");
                equal(response.session.showCorrect, false, "Correct showCorrect is fetched");
                equal(response.session.hideUserDetails, true, "Correct hideUserDetails is fetched");
                equal(response.session.enableComments, true, "Correct enableComments is fetched");
                equal(response.session.enableRatings, true, "Correct enableRatings is fetched");
                equal(response.session.requireRatings, true, "Correct requireRatings is fetched");
                equal(response.session.timed, false, "Correct timed is fetched");
                getSessions0();
            }
        );
    }

    function getSessions0() {
        var activity = state.activity0;
        servicesTest('get activity sessions for activity0, non-admin', 'session/getSessions', 'POST', state.user5,
            {
                activity: activity.UUID
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 31, "Correct error code returned");
                getSessions1();
            }
        );
    }

    function getSessions1() {
        var activity = state.activity0;
        servicesTest('get activity sessions for activity0, admin', 'session/getSessions', 'POST', state.user0,
            {
                activity: activity.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, true, "session0 returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, true, "session1 returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, true, "session2 returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions2();
            }
        );
    }
    function getSessions2() {
        var activity = state.activity0;
        var group = state.group0;
        servicesTest('get group and activity sessions for activity0, group0, non-group member', 'session/getSessions', 'POST', state.user9,
            {
                activity: activity.UUID,
                group: group.UUID
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code returned");
                getSessions3();
            }
        );
    }
    function getSessions3() {
        var activity = state.activity0;
        var group = state.group0;
        servicesTest('get group and activity sessions for activity0, group0, group member', 'session/getSessions', 'POST', state.user4,
            {
                activity: activity.UUID,
                group: group.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, true, "session0 returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, false, "session1 not returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, false, "session2 not returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions4();
            }
        );
    }
    function getSessions4() {
        var activity = state.activity0;
        var group = state.group1;
        servicesTest('get group and activity sessions for activity0, group1, group member', 'session/getSessions', 'POST', state.user8,
            {
                activity: activity.UUID,
                group: group.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, false, "session0 not returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, false, "session1 not returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, true, "session2 returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions5();
            }
        );
    }
    function getSessions5() {
        var activity = state.activity0;
        var group = state.group1;
        servicesTest('get group and activity sessions for activity0, group1, group organizer', 'session/getSessions', 'POST', state.user7,
            {
                activity: activity.UUID,
                group: group.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, false, "session0 not returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, true, "session1 returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, true, "session2 returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions6();
            }
        );
    }
    function getSessions6() {
        var group = state.group1;
        servicesTest('get group sessions for group1, group organizer', 'session/getSessions', 'POST', state.user7,
            {
                group: group.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, false, "session0 not returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, true, "session1 returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, true, "session2 returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions7();
            }
        );
    }
    function getSessions7() {
        var group = state.group1;
        servicesTest('get group sessions for group1, group member', 'session/getSessions', 'POST', state.user8,
            {
                group: group.UUID
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.sessions, "UUID");
                equal(UUIDs.indexOf(state.session0.UUID) > -1, false, "session0 not returned.");
                equal(UUIDs.indexOf(state.session1.UUID) > -1, false, "session1 not returned.");
                equal(UUIDs.indexOf(state.session2.UUID) > -1, true, "session2 returned.");
                equal(UUIDs.indexOf(state.session3.UUID), -1, "session3 not returned.");
                getSessions8();
            }
        );
    }
    function getSessions8() {
        var activity = state.activity0;
        var group = state.group1;
        servicesTest('get group sessions for group1, non-group member', 'session/getSessions', 'POST', state.user2,
            {
                group: group.UUID
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 30, "Correct error code returned");
                callback && callback();
            }
        );
    }
    function cleanUp() {
        login(state.user0, function() {
            //delete sessions
            var toDelete = [ state.session0, state.session1, state.session2 ];
            var deleteCount = toDelete.length;
            for (var i = 0, l = deleteCount; i < l; i++) {
                var session = toDelete[i];
                $.ajax({
                    url: server + "session/" + session.UUID,
                    type: 'DELETE',
                    complete: function (result, status) {
                        deleteCount--;
                        if (deleteCount < 1) {
                            //callback && callback();
                        }
                    }
                });
            }
        });
    }
}