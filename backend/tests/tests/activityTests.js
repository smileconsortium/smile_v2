//activity0 is owned by user7, group0 (user7 is only a member in group0), and is private
//activity1 is owned by user9, group0, and is public
//activity2 - clone of activity0, no group
//activity3 - clone of activity0, no group
//activity4 - clone of activity0, group1

window.activityTests = function (callback) {
    //load model
    state.activity0 = {
        name: "Intro Quiz",
        description: "",
        activityType: "flippedMC",
        owner: state.user2.UUID,
        owningGroup: state.group1.UUID,
        institution: state.institution0.UUID,
        createdOn: [2013, 12, 2, 0, 37, 0],
        public: true,
        tags: ["science", "grade:12", "chemistry", "AP chemistry", "AP chem", "atomic chemistry", "orbitals"],
        gradeLevels: ["12"],
        subjects: ["chemistry", "science", "physical sciences"],
        language: "English"
    };


    state.activity1 = {
        name: "Properties of Atoms",
        description: "",
        activityType: "exam",
        owner: state.user9.UUID,
        owningGroup: state.group0.UUID,
        institution: state.institution0.UUID,
        createdOn: [2013, 12, 2, 0, 40, 42],
        public: true
    };

    createActivity0();

    function createActivity0() {
        var activity = state.activity0;
        servicesTest('create activity0 - non-admin user with different owner', 'activity', 'POST', state.user7,
            activity,
            false,
            "success",
            function (response) {
                equal(response.error && response.error.code, 23, "Correct error code returned.");
                createActivity1();
            }
        );
    }

    function createActivity1() {
        var activity = state.activity0;
        servicesTest('create activity0 - non-admin user, without owner field, with owningGroup that is NOT an organizer in',
            'activity', 'POST', state.user3,
            _.omit(activity, "owner"),
            false,
            "success",
            function (response) {
                if (response && response.UUID) activity.UUID = response.UUID;
                equal(response.error && response.error.code, 24, "Correct error code returned.");
                createActivity2();
            }
        );
    }

    function createActivity2() {
        var activity = state.activity0;
        servicesTest('create activity0 - non-admin user, without owner field, with owningGroup that is an organizer in',
            'activity', 'POST', state.user7,
            _.omit(activity, "owner"),
            true,
            "success",
            function (response) {
                if (response && response.UUID) activity.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createActivity3();
            }
        );
    }

    function createActivity3() {
        var activity = state.activity1;
        servicesTest('create activity1 - admin user', 'activity', 'POST', state.user0,
            activity,
            true,
            "success",
            function (response) {
                if (response && response.UUID) activity.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                updateActivity0();
            }
        );
    }

    function updateActivity0() {
        var activity = state.activity0;
        servicesTest('update activity - change owner as organizer in current owningGroup',
            'activity/' + activity.UUID, 'PUT', state.user5,
            {
                description: "My description is this.",
                activityType: "openDiscussion",
                owner: state.user1.UUID,
                owningGroup: state.group1.UUID,
                createdOn: [2019, 12, 2, 0, 37, 0],
                public: false,
                tags: ["science"],
                newField: ["Storing some", "array", "data"]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 23, "Correct error code returned.");
                updateActivity1();
            }
        );
    }

    function updateActivity1() {
        var activity = state.activity0;
        servicesTest('update activity - organizer in current owningGroup, change to owning group that is not an organizer of',
            'activity/' + activity.UUID, 'PUT', state.user6,
            {
                description: "My description is this.",
                activityType: "openDiscussion",
                owningGroup: state.group0.UUID,
                createdOn: [2019, 12, 2, 0, 37, 0],
                public: false,
                tags: ["science"],
                newField: ["Storing some", "array", "data"]
            }, false, "success",
            function (response) {
                equal(response.error && response.error.code, 24, "Correct error code returned.");
                updateActivity2();
            }
        );
    }

    function updateActivity2() {
        var activity = state.activity0;
        servicesTest('update activity - organizer in current owningGroup, change to owning group that is an organizer of',
            'activity/' + activity.UUID, 'PUT', state.user5,
            {
                description: "My description is this.",
                activityType: "openDiscussion",
                owningGroup: state.group0.UUID,
                createdOn: [2019, 12, 2, 0, 37, 0],
                public: false,
                tags: ["science"],
                newField: ["Storing some", "array", "data"]
            }, true, "success",
            function (response) {
                getActivity0();
            }
        );
    }

    function getActivity0() {
        var activity = state.activity0;
        servicesTest('get activity, not a group member in session\'s owning group + activity is private',
            'activity/' + activity.UUID, 'GET', state.user9, null, false, "success",
            function (response) {
                equal(response.error && response.error.code, 24, "Insufficient permissions to view error code returned.");
                getActivity1();
            }
        );
    }

    function getActivity1() {
        var activity = state.activity1;
        servicesTest('get activity, not a group member in session\'s owning group + activity is public',
            'activity/' + activity.UUID, 'GET', state.user5, null, true, "success",
            function (response) {
                equal(response.activity.UUID, activity.UUID, "Correct UUID is fetched");
                equal(response.activity.activityType, activity.activityType, "Correct activityType is fetched");
                equal(response.activity.owningGroup, activity.owningGroup, "Correct owningGroup is fetched");
                equal(response.activity.owner, activity.owner, "Correct owner is fetched");
                equal(response.activity.language, activity.language, "Correct owner is fetched");
                getActivity2();
            }
        );
    }

    function getActivity2() {
        var activity = state.activity0;
        servicesTest('get activity, not a group member', 'activity/' + activity.UUID, 'GET', state.user5, null,
            true, "success",
            function (response) {
                equal(activity.UUID, response.activity.UUID, "Correct UUID is fetched");
                equal(response.activity.activityType, "openDiscussion", "Correct activityType is fetched");
                equal(_.isEqual(response.activity.newField, ["Storing some", "array", "data"]),
                    true,  "newField (newly added field) is added");
                equal(response.activity.owningGroup, state.group0.UUID, "Correct owningGroup is fetched");
                equal(response.activity.owner, state.user2.UUID, "Correct owner is fetched");
                equal(response.activity.language, "English", "Correct language is fetched - (old unchanged field)");
                getGroupActivities0();
            }
        );
    }
    function getGroupActivities0() {
        var group = state.group0;
        servicesTest('get group activities - not a group organizer', 'activity/groupActivities/' + group.UUID, 'GET', state.user7, null,
            false, "success",
            function (response) {
                equal(response.error && response.error.code, 25, "Correct error code returned");
                getGroupActivities1();
            }
        );
    }
    function getGroupActivities1() {
        var group = state.group0;
        servicesTest('get group activities - group organizer', 'activity/groupActivities/' + group.UUID, 'GET', state.user5, null,
            true, "success",
            function (response) {
                var activityIds = _.pluck(response.activities, "UUID");
                equal(activityIds.indexOf(state.activity0.UUID) > -1, true, "activity0 is returned");
                equal(activityIds.indexOf(state.activity1.UUID) > -1, true, "activity1 is returned");
                console.log(response);
                getGroupActivities2();
            }
        );
    }
    function getGroupActivities2() {
        var group = state.group1;
        servicesTest('get group activities - group organizer', 'activity/groupActivities/' + group.UUID, 'GET', state.user7, null,
            true, "success",
            function (response) {
                equal(response.activities && response.activities.length, 0, "No activities returned");
                getUserActivities0();
            }
        );
    }
    function getUserActivities0() {
        var user = state.user5;
        servicesTest('get user activities - for different user, non-admin', 'activity/userActivities/' + user.UUID, 'GET', state.user6, null,
            false, "success",
            function (response) {
                equal(response.error && response.error.code, 26, "Correct error code returned");
                getUserActivities1();
            }
        );
    }
    function getUserActivities1() {
        var user = state.user7;
        servicesTest('get user activities - for different user, admin', 'activity/userActivities/' + user.UUID, 'GET', state.user0, null,
            true, "success",
            function (response) {
                var activityIds = _.pluck(response.activities, "UUID");
                equal(activityIds.indexOf(state.activity0.UUID) > -1, true, "activity0 is returned");
                equal(response.activities && response.activities.length, 1, "Only activity0 is returned.");
                getUserActivities2();
            }
        );
    }
    function getUserActivities2() {
        var user = state.user5;
        servicesTest('get user activities - for same user', 'activity/userActivities/' + user.UUID, 'GET', user, null,
            true, "success",
            function (response) {
                var activityIds = _.pluck(response.activities, "UUID");
                equal(activityIds.indexOf(state.activity1.UUID) > -1, true, "activity1 is returned");
                equal(response.activities && response.activities.length, 1, "Only activity1 is returned.");
                getUserActivities3();
            }
        );
    }
    function getUserActivities3() {
        var user = state.user6; //only in group0
        servicesTest('get user activities - user doesn\'t own any activities', 'activity/userActivities/' + user.UUID, 'GET', user, null,
            true, "success",
            function (response) {
                equal(response.activities && response.activities.length, 0, "No activities returned");
                cloneActivity0();
            }
        );
    }
    function cloneActivity0() {
        var activity = state.activity0; //only in group0
        servicesTest('clone private activity - (no owningGroup) not admin, not owner, not in owning group', 'activity/' + activity.UUID + "/clone", 'POST', state.user9, {

            },
            false, "success",
            function (response) {
                equal(response.error && response.error.code, 37, "Correct error code returned");
                cloneActivity1();
            }
        );
    }
    function cloneActivity1() {
        var activity = state.activity0; //only in group0
        var newActivity = state.activity2 = {};


        servicesTest('clone private activity - (no owningGroup) not admin, not owner, in owning group', 'activity/' + activity.UUID + "/clone", 'POST', state.user7, {

            },
            true, "success",
            function (response) {
                if (response && response.UUID) newActivity.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                cloneActivity2();
            }
        );
    }
    function cloneActivity2() {
        var activity = state.activity0; //only in group0
        var newActivity = state.activity3 = {};

        servicesTest('clone private activity - (no owningGroup) admin, not in group', 'activity/' + activity.UUID + "/clone", 'POST', state.user0, {

            },
            true, "success",
            function (response) {
                if (response && response.UUID) newActivity.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                cloneActivity3();
            }
        );
    }
    function cloneActivity3() {
        var activity = state.activity0; //only in group0
        servicesTest('clone private activity - (new owningGroup - group1) not admin, not owner, not in owning group', 'activity/' + activity.UUID + "/clone", 'POST', state.user9, {

            },
            false, "success",
            function (response) {
                equal(response.error && response.error.code, 37, "Correct error code returned");
                cloneActivity4();
            }
        );
    }
    function cloneActivity4() {
        var activity = state.activity0; //only in group0
        var newActivity = state.activity4 = {};
        var newOwningGroup = state.group1;

        servicesTest('clone private activity - (new owningGroup - group1) not admin, not owner, in owning group', 'activity/' + activity.UUID + "/clone", 'POST', state.user7, {
                owningGroup: newOwningGroup.UUID
            },
            true, "success",
            function (response) {
                if (response && response.UUID) newActivity.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");

                checkClone();
            }
        );
    }
    function checkClone() {
        var newOwningGroup = state.group1;

        var activity = state.activity4;
        servicesTest('test cloned activity', 'activity/' + activity.UUID, 'GET', null, null, true, "success", function (response) {
            var activity = response && response.activity;
            equal(activity.owner, state.user7.UUID, "Correct owner UUID");
            equal(activity.owningGroup, newOwningGroup.UUID, "Correct owningGroup UUID");

            callback && callback();
        });
    }
}