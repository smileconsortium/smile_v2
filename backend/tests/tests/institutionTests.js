window.institutionTests = function(callback) {
    //load model
    state.institution0 = {
        name: "SMILEConsortium",
        institutionType: "Non-profit",
        country: "United States",
        region: "California",
        city: "Los Altos",
        website: "www.smileconsortium.org"
    };

    state.institution1 = {
        name: "Gotham",
        institutionType: "Government",
        country: "United States",
        region: "New York"
    };

    //delete existing institution if exists
    login(state.user2, function () {
        var institution = state.institution0;
        if (!institution.UUID) {
            $.ajax({
                url: server + "institution/getByName/" + institution.name,
                type: 'GET',
                complete: function (result, status) {
                    if (status == "success") {
                        var response = JSON.parse(result.responseText);
                        institution.UUID = response.institution.UUID;
                        deleteInst0(response.institution.UUID);
                    } else {
                        getInst1ToDelete();
                    }
                }
            });
        } else {
            deleteInst0(institution.UUID);
        }
    });

    function deleteInst0(UUID) {
        login(state.user0, function () {
            $.ajax({
                url: server + "institution/" + UUID,
                type: 'DELETE',
                complete: function (result, status) {
                    getInst1ToDelete();
                }
            });
        });
    }
    function getInst1ToDelete() {
    //delete existing institution if exists
        login(state.user2, function () {
            var institution = state.institution1;
            if (!institution.UUID) {
                $.ajax({
                    url: server + "institution/getByName/" + institution.name,
                    type: 'GET',
                    complete: function (result, status) {
                        if (status == "success") {
                            var response = JSON.parse(result.responseText);
                            institution.UUID = response.institution.UUID;
                            deleteInst1(response.institution.UUID);
                        } else {
                            createInst0();
                        }
                    }
                });
            } else {
                deleteInst1(institution.UUID);
            }
        });
    }

    function deleteInst1(UUID) {
        servicesTest('delete institution - admin', 'institution/' + UUID, 'DELETE', state.user0, {
        }, true, "success", function (response) {
            createInst0();
        });
    }


    function createInst0() {
        var institution = state.institution0;
        servicesTest('create institution0 - non-admin user', 'institution', 'POST', state.user7,
            _.omit(institution, 'UUID'),
            true,
            "success",
            function (response) {;
                if (response && response.UUID) institution.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                createInst1();
            }
        );
    }

    function createInst1() {
        var institution = state.institution1;
        servicesTest('create institution1 - non-admin user', 'institution', 'POST', state.user7,
            _.omit(institution, 'UUID'),
            true,
            "success",
            function (response) {;
                if (response && response.UUID) institution.UUID = response.UUID;
                equal(_.has(response, 'UUID'), true, "UUID included in response");
                getInstByName();
            }
        );
    }

    function getInstByName() {
        var institution = state.institution0;
        servicesTest('institution getByName', 'institution/getByName/' + institution.name, 'GET', null,
            null, true, "success",
            function (response) {
                equal(institution.UUID, response.institution.UUID, "Correct UUID is fetched");
                updateInst();
            }
        );
    }
    function updateInst() {
        var institution = state.institution0;
        servicesTest('update institution', 'institution/' + institution.UUID, 'PUT', null,
            {
                institutionType: "NGO",
                website: "www.smilec.org",
                sector: "EdTech"
            }, true, "success",
            function (response) {
                getInst();
            }
        );
    }
    function getInst() {
        var institution = state.institution0;
        servicesTest('get institution', 'institution/' + institution.UUID, 'GET', null,
            null, true, "success",
            function (response) {
                equal(institution.UUID, response.institution.UUID, "Correct UUID is fetched");
                equal(response.institution.institutionType, "NGO", "Correct institutionType is fetched");
                equal(response.institution.website, "www.smilec.org", "Correct website is fetched");
                equal(response.institution.sector, "EdTech", "Correct sector is fetched (newly added field)");
                equal(response.institution.country, "United States", "Correct UUID is fetched - (old unchanged field)");
                getInstNamesFromUUID();
            }
        );
    }
    function getInstNamesFromUUID() {
        servicesTest('get institution names from UUIDs', 'institution/getNamesFromUUID', 'POST', null,
            {
                UUIDs: [ state.institution0.UUID, state.institution1.UUID ]
            }, true, "success",
            function (response) {
                var UUIDs = _.pluck(response.institutions, "UUID");
                var names = _.pluck(response.institutions, "name");
                equal(UUIDs.indexOf(state.institution0.UUID) > -1, true);
                equal(UUIDs.indexOf(state.institution0.UUID) > -1, true);
                equal(names.indexOf(state.institution0.name) > -1, true);
                equal(names.indexOf(state.institution0.name) > -1, true);
                instAutoFill();
            }
        );
    }
    function instAutoFill() {
        servicesTest('institution name auto fill', 'institution/nameAutoFill&name=' + 'Goth', 'GET', null,
            null, null, null,
            function (response) {
                var UUIDs = _.pluck(response.institutions, "UUID");
                var names = _.pluck(response.institutions, "name");
                equal(UUIDs.indexOf(state.institution1.UUID) > -1, true);
                equal(names.indexOf(state.institution1.name) > -1, true);
                callback && callback();
            }
        );
    }
}