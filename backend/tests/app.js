(function (window) {
    var app = window.app = {
        apiRoot: 'http://localhost:1337/',
        init: function () {

        }
    };

    //modify javascript native types
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }



    $(function () {
        app.init();
    });
}) (window);