(function(app) {
    app.SettingsView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.SettingsView.prototype.template = this.template || _.template($('#settings-template').html());
        },

        render: function() {
            this.$el.html(this.template());
            logger.info("SettingView rendered");

            return this;
        },

        remove: function() {
            logger.info("SettingView removed");
            this.parentRemove();
        }
    })
})(app);