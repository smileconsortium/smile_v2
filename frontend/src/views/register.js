(function(app) {
    var url = app.apiRoot + "user";
    app.RegisterView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.RegisterView.prototype.template = this.template || _.template($('#register-template').html());
        },

        events: {
            'submit #register-form': 'register'
        },

        render: function() {
            var signin = app.lang.get("signin"),
                register = app.lang.get("register"),
                signinTitle = app.lang.get("signinTitle"),
                registerTitle = app.lang.get("registerTitle"),
                forgotPw = app.lang.get("forgotPassword"),
                forgotUn = app.lang.get("forgotUsername");
            this.$el.html(this.template({signin: signin, register: register, signinTitle: signinTitle,
                registerTitle: registerTitle, forgotPw: forgotPw, forgotUn: forgotUn}));
            logger.info("RegisterView rendered");

            return this;
        },

        remove: function() {
            logger.info("RegisterView removed");
            this.parentRemove();
        },

        register:function (event) {
            event.preventDefault(); // Don't let this button submit the form
            $('.alert-error').hide(); // Hide any errors on a new submit

            logger.info('Logging in... ');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',

                data: JSON.stringify({
                    username: $('#register-username').val(),
                    password: $('#register-password').val(),
                    firstName: $('#register-firstName').val(),
                    lastName: $('#register-lastName').val(),
                    emailAddresses: [ "pedro.bispo@neticle.com", "pedro.bispo@neticle.pt" ],
                    admin: false,
                    country: "Portugal",
                    region: "Ilha Terceira, Azores",
                    city: "Praia da Vitória",
                    roles: ["student"],
                    grade: 12,
                    institutions: ["1234i1288", "12349001"],
                    language: "English"
                }),

                success: function () {
                    console.log('Registered User successfully!');
                },
                error: function() {
                    console.log('Failed to register User!');
                }
            });
        },

        message: function(title, msg) {
            logger.info("Register View: message(" + title + ": " + msg + ")");
        }
    })
})(app);

