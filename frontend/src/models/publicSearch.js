(function () {
    /*
     questions
     searchQuery
     orderId
     start
     range

     */
    app.PublicSearchModel = app.Model.extend({
        fetchQuestions: function (options) {
            options = options || {};
            console.log(options);
            var questions = this.get('questions') || new app.Question.Collection({searchText: this.get("searchText")});
            console.log(questions);
            logger.info("Fetching public question collection from server");

            var that = this;
            console.log(this.get("searchText"));
            questions.fetch({
                searchText: options.searchText || "",
                orderCode: options.orderCode || null,
                start: options.start || null,
                range: options.range || null,
                showOwn: options.showOwn || null,
                showAnswered: options.showAnswered || null,
                success: function (collection, response) {
                    /*TODO: Remove this temporary bullshit
                     //console.log("Temporarily fucking with these image URL's");
                     _.each(collection.models, function(element, index, list) {
                     var avatar = "" + element.get('avatar_path').replace(":8080", "");
                     element.set("avatar_path", avatar);
                     //console.log(element);
                     });*/
                    that.set({questions: collection});
                    //TODO: Don't know why I need this but apparently I do
                    //TODO: learn backbone internals better
                    //see: http://stackoverflow.com/questions/9909799/backbone-js-change-not-firing-on-model-change
                    that.trigger("change:questions");
                },

                error: function (collection, response) {
                    that.fetchError("Error fetching questions. ", response, function () {

                    });
                }
            }, options);
        }
    });
})();