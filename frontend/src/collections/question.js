(function () {
    var Question = app.Question = app.Question || {};

    Question.Collection = app.Collection.extend({
        initialize: function() {
            console.log("GOOD MORNING VIETNAM");
        },
        url: function () {
            return this.urlRoot + "question/public"
        },

        urlRoot: app.apiRoot,

        parse: function (response) {
            return response;
        },

        sync: function (method, model, options) {
            console.log("OPTIONS");
            console.log(options);
            //set default options
            options.searchText = options.searchText || "";
            options.orderCode = options.orderCode || 3;
            options.start = options.start || 1;
            options.range = options.range || 10;
            options.showOwn = options.showOwn || 1;
            options.showAnswered = options.showAnswered || 1;

            this.json = JSON.stringify({searchText: options.searchText, orderCode: options.orderCode, start: options.start, range: options.range, showOwn: options.showOwn, showAnswered: options.showAnswered});

            var that = this;
            var params = _.extend({
                type: 'POST',
                contentType: 'application/json;charset=UTF-8',
                data: that.json,
                dataType: 'json',
                url: that.url(),
                processData: true
            }, options);

            return $.ajax(params);
        }
    });
})();