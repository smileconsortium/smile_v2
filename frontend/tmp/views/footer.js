(function (app) {
    app.FooterView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.FooterView.prototype.template = this.template || _.template($('#footer-template').html());

        },
        render: function () {
            this.$el.html(this.template());
            logger.info("FooterView rendered");

            return this;
        },

        remove: function () {
            logger.info("FooterView removed");
            this.parentRemove();
        }
    });
})(app);

