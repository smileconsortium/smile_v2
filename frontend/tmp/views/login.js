(function(app) {
    var url = app.apiRoot + "auth/login";
    app.LoginView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.LoginView.prototype.template = this.template || _.template($('#login-template').html());
        },

        events: {
            'submit #login-form': 'login'
        },

        render: function() {
            var signin = app.lang.get("signin"),
                register = app.lang.get("register"),
                signinTitle = app.lang.get("signinTitle"),
                registerTitle = app.lang.get("registerTitle"),
                forgotPw = app.lang.get("forgotPassword"),
                forgotUn = app.lang.get("forgotUsername");
            this.$el.html(this.template({signin: signin, register: register, signinTitle: signinTitle,
                registerTitle: registerTitle, forgotPw: forgotPw, forgotUn: forgotUn}));
            logger.info("LoginView rendered");

            return this;
        },

        remove: function() {
            logger.info("LoginView removed");
            this.parentRemove();
        },

        login:function (event) {
            event.preventDefault(); // Don't let this button submit the form
            $('.alert-error').hide(); // Hide any errors on a new submit

            logger.info('Logging in... ');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',

                data: JSON.stringify({
                    username: $('#signin-username').val(),
                    password: $('#signin-password').val()
                }),

                success: function () {
                    
                },
                error: function() {
                    
                },
                complete: function (result) {
                    
                    

                    if(result.responseText) {  // If there is an error, show the error messages
                        var userObj = JSON.parse(result.responseText);

                        if (userObj.success === true) {
                            
                            var user = new app.User.Model({id: userObj.UUID});

                            user.fetch({
                                success: function (user) {
                                    
                                    app.setUser(user);
                                    app.router.navigate("home", {trigger: true, replace: false});
                                },
                                error: function(user) {
                                    
                                }
                            });

                        } else {
                            this.message("Server Error", "Can't login. Please try again later.");
                        }
                    }
                    else { // If not, send them back to the home page
                        this.message("Server Error", "Can't login. Please try again later.");
                    }
                }
            });
        },

        message: function(title, msg) {
            logger.info("Login View: message(" + title + ": " + msg + ")");
        }
    })
})(app);

