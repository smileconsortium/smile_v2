(function (window) {
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var app = window.app = {
        developmentMode: true,

        apiRoot: 'http://localhost/services/',

        getUser: function () {
            //var user = app.cache.get("user");
            var user = app.user;
            //if (user instanceof Backbone.Model == false) user = new app.User.Model(user);
            if (user) return user;
            if (typeof(user) === 'undefined' || user === "undefined") {
                user = undefined;
                //if user is undefined, route to login
                app.router.login();
                return user;
            }
        },
        setUser: function (user) {
           // this.cache.set('user', user);
            this.user = user;
        },

        init: function () {
            
             //insert dummy logger object for production only
             var logger = window.logger = {
                 error: function() { return false; },
                 info: function() { return false; },
                 debug: function() { return false; },
                 log: function() { return false; },
                 trace: function() { return false; },
                 warn: function() { return false; }
             }
             

            

            logger.info("app initialized");

            //disable animation for android since hardware is uncertain
            if (app.is_android) {
                $('#app-container').addClass('no-transition');
                $('#menu-container').addClass('no-transition');
                $('#menu-overlay').addClass('no-transition');
                app.no_transition = true;
            } else {
                app.no_transition = false;
            }
            //add menu animation style
            $('#menu-container').addClass(app.settings.get("menu_animation"));

            app.router = new app.Router({container: 'app-container'});
            app.dispatcher = _.clone(Backbone.Events);
            app.navbar_visible = false;
            //Shared templates
            app.loadingTemplate = _.template($('#loading-template').html());

            Backbone.history.start();
        }
    };

    //modify javascript native types
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    //user-agent detection
    var agent = navigator.userAgent.toLowerCase();
    
    app.mobile_agent = (agent.indexOf("mobi") > -1);
    app.is_android = (agent.indexOf("android") > -1);

    /*
    //configure storage
    window.onload = function() {
        
        app.cache.restoreAll();
    }
    window.onunload = function() {
        
        app.cache.persistAll();
    }
    */

    
    //for production, attach templates to index.html via Jakefile
    $(app.init);
}) (window);