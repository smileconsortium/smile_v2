(function () {
    /*
     questions
     searchQuery
     orderId
     start
     range

     */
    app.PublicSearchModel = app.Model.extend({
        fetchQuestions: function (options) {
            options = options || {};
            
            var questions = this.get('questions') || new app.Question.Collection({searchText: this.get("searchText")});
            
            logger.info("Fetching public question collection from server");

            var that = this;
            
            questions.fetch({
                searchText: options.searchText || "",
                orderCode: options.orderCode || null,
                start: options.start || null,
                range: options.range || null,
                showOwn: options.showOwn || null,
                showAnswered: options.showAnswered || null,
                success: function (collection, response) {
                    /*TODO: Remove this temporary bullshit
                     //
                     _.each(collection.models, function(element, index, list) {
                     var avatar = "" + element.get('avatar_path').replace(":8080", "");
                     element.set("avatar_path", avatar);
                     //
                     });*/
                    that.set({questions: collection});
                    //TODO: Don't know why I need this but apparently I do
                    //TODO: learn backbone internals better
                    //see: http://stackoverflow.com/questions/9909799/backbone-js-change-not-firing-on-model-change
                    that.trigger("change:questions");
                },

                error: function (collection, response) {
                    that.fetchError("Error fetching questions. ", response, function () {

                    });
                }
            }, options);
        }
    });
})();